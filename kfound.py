#!/usr/bin/env python3
import os
from telegram import Bot

def send_message_to_telegram(bot_token, chat_id, message):
    bot = Bot(token=bot_token)
    bot.send_message(chat_id=chat_id, text=message)

def send_file_contents_to_telegram(file_path, bot_token, chat_id):
    if os.path.isfile(file_path):
        with open(file_path, 'r') as file:
            file_contents = file.read()

        send_message_to_telegram(bot_token, chat_id, file_contents)
    else:
        print("File does not exist.")

def send_link_to_telegram(link, bot_token, chat_id):
    send_message_to_telegram(bot_token, chat_id, link)

# Replace 'TOKEN' with your Telegram bot token
bot_token = '6179762759:AAGG5s0wZ1cIKzyIQGJyDhYXZRWJAOkC0bg'
# Replace 'CHAT_ID' with the chat ID you want to send the file contents and link to
chat_id = '1076817879'
# Replace 'PATH/TO/FILE' with the actual file path
file_path = '~azharsalip/keyhunt/KEYFOUNDKEYFOUND.txt'
# Replace 'LINK' with the actual link you want to send
link = 'https://tenor.com/view/bitcoin-gif-11349536'

send_file_contents_to_telegram(file_path, bot_token, chat_id)
send_link_to_telegram(link, bot_token, chat_id)

